package io.obacht.ar.augmented.ar.base

import io.obacht.ar.augmented.architecture.base.view.fragment.BaseFragment

abstract class ArFragment<S : IArState, VM : ArViewmodel<S>> : BaseFragment<S, VM>() {
    abstract val sceneViewId: Int
}