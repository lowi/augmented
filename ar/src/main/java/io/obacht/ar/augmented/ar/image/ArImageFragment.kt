package io.obacht.ar.augmented.ar.image

import io.obacht.ar.augmented.ar.base.ArFragment

abstract class ArImageFragment<S : IArImageState, VM : ArImageViewmodel<S>> : ArFragment<S, VM>()