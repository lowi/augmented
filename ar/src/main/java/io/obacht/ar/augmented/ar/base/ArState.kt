package io.obacht.ar.augmented.ar.base

import arrow.core.Option
import com.google.ar.core.Session
import com.google.ar.sceneform.rendering.ModelRenderable
import io.obacht.ar.augmented.architecture.base.viewstate.IViewState

data class ArState(

    // for when we check whether the ar.core-apk is installed
    val apkInstallRequested: Boolean = false,

    // the ar.core-session. we need this all over the place
    // …or do we? I think sceneform handles this for us.
    // todo: check for removal
    val arSession: Option<Session> = Option.empty(),

    // here we map some identifier to a renderable.
    // the id can be anything, but should probably resemble the filename or something
    val renderables: Map<String, ModelRenderable> = HashMap(),

    ) : IViewState