package io.obacht.ar.augmented.ar.base

import io.obacht.ar.augmented.architecture.base.viewstate.IViewState

interface IArState : IViewState