package io.obacht.ar.augmented.ar.image

import io.obacht.ar.augmented.ar.base.ArViewmodel
import io.obacht.ar.augmented.domain.ImageDbRepository
import io.obacht.ar.augmented.domain.RenderableRepository

abstract class ArImageViewmodel<S : IArImageState>(
    fileNames: Map<String, String>,
    renderableRepository: RenderableRepository,
    protected val imageDbRepository: ImageDbRepository
) : ArViewmodel<S>(fileNames, renderableRepository)