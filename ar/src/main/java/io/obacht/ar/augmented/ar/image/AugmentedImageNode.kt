package io.obacht.ar.augmented.ar.image

import com.google.ar.core.AugmentedImage
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.rendering.ModelRenderable


/**
 * Node for rendering an augmented image.
 */
class AugmentedImageNode(
    model: ModelRenderable,
    image: AugmentedImage,
) : AnchorNode() {

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image.
     */
    init {

        // Set the anchor based on the center of the image.
        anchor = image.createAnchor(image.centerPose)

        // Upper left corner.
//        val localPosition = Vector3()
//        localPosition[-0.5f * image.extentX, 0.0f] = -0.5f * image.extentZ

        // create a new scene-node
        val cornerNode = Node()
        cornerNode.setParent(this)
//        cornerNode.localPosition = localPosition
        cornerNode.renderable = model
    }
}
