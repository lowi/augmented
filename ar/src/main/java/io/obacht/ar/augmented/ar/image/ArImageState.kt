package io.obacht.ar.augmented.ar.image

import arrow.core.Option
import io.obacht.ar.augmented.ar.base.ArState
import io.obacht.ar.augmented.domain.ImageSceneMap

data class ArImageState(
    val arState: ArState = ArState(),
    val sceneMap: Option<ImageSceneMap> = Option.empty(),
) : IArImageState
