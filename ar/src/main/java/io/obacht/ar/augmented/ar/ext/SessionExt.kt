package io.obacht.ar.augmented.ar.ext

import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Config
import com.google.ar.core.Session
import com.google.ar.core.exceptions.*
import io.obacht.ar.augmented.domain.ArError


fun Session.konfigure(images: AugmentedImageDatabase): Session =
    try {
        val config = this.config

        // configure depth API.
        // (Always disabled since my phone doesn't support it :( )
        if (this.isDepthModeSupported(Config.DepthMode.AUTOMATIC)) {
            config.depthMode = Config.DepthMode.AUTOMATIC
        } else {
            config.depthMode = Config.DepthMode.DISABLED
        }

        // Force the non-blocking mode for the session.
        config.updateMode = Config.UpdateMode.LATEST_CAMERA_IMAGE

        // enable auto-focus
        config.focusMode = Config.FocusMode.AUTO

        // set light estimation strategy
        config.lightEstimationMode = Config.LightEstimationMode.ENVIRONMENTAL_HDR
//        config.lightEstimationMode = Config.LightEstimationMode.AMBIENT_INTENSITY

        // set the image database
        config.augmentedImageDatabase = images

        // actual configuration!
        this.configure(config)

        // return the session
        this

    } catch (e: UnavailableException) {
        throw ArError.ArCore.NotInstalled
    } catch (e: UnavailableUserDeclinedInstallationException) {
        throw ArError.ArCore.NotInstalled
    } catch (e: UnavailableApkTooOldException) {
        throw ArError.ArCore.OutOfDate
    } catch (e: UnavailableSdkTooOldException) {
        throw ArError.ArCore.OutOfDate
    } catch (e: UnavailableDeviceNotCompatibleException) {
        throw ArError.ArCore.NotSupported
    } catch (e: Exception) {
        throw ArError.ArCore.Unknown
    }
