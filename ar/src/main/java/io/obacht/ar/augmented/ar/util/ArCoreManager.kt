package io.obacht.ar.augmented.ar.util

import android.app.Activity
import android.content.Context
import arrow.fx.IO
import com.google.ar.core.ArCoreApk
import com.google.ar.core.ArCoreApk.InstallStatus
import io.obacht.ar.augmented.domain.ArError

/**
 * Enum class for ArCore installation requests.
 */
enum class ArInstallResponse {
    INSTALL_REQUESTED,
    INSTALLED
}

object ArCoreManager {

    fun checkAvailability(context: Context): IO<Unit> =

        when (ArCoreApk.getInstance().checkAvailability(context)) {
            ArCoreApk.Availability.SUPPORTED_INSTALLED -> IO.unit

            ArCoreApk.Availability.UNKNOWN_ERROR -> IO.raiseError(ArError.ArCore.Apk.Unknown)
            ArCoreApk.Availability.UNKNOWN_CHECKING -> IO.raiseError(ArError.ArCore.Apk.UnknownChecking)
            ArCoreApk.Availability.UNKNOWN_TIMED_OUT -> IO.raiseError(ArError.ArCore.Apk.UnknownTimeout)
            ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE -> IO.raiseError(ArError.ArCore.Apk.DeviceIncapable)
            ArCoreApk.Availability.SUPPORTED_NOT_INSTALLED -> IO.raiseError(ArError.ArCore.Apk.NotInstalled)
            ArCoreApk.Availability.SUPPORTED_APK_TOO_OLD -> IO.raiseError(ArError.ArCore.Apk.TooOld)
            null -> IO.raiseError(ArError.ArCore.Apk.Unknown)
        }


    fun installArCore(
        activity: Activity,
        apkInstallRequested: Boolean = false
    ): IO<ArInstallResponse> =

    //    Initiates installation of ARCore when needed. When your application launches or
    //    enters an AR mode, it should call this method with userRequestedInstall = true.

    //    If ARCore is installed and compatible, this function will return INSTALLED.

    //    If ARCore is not currently installed or the installed version is not compatible,
    //    the function will return INSTALL_REQUESTED immediately.
    //    Your current activity will then pause while the user is offered the opportunity to install it.

    //    When your activity resumes, you should call this method again, this time with
    //    userRequestedInstall = false. This will either return INSTALLED or throw an exception
    //    indicating the reason that installation could not be completed.

        // @see https://developers.google.com/ar/reference/java/arcore/reference/com/google/ar/core/ArCoreApk
        when (ArCoreApk.getInstance().requestInstall(activity, !apkInstallRequested)) {
            InstallStatus.INSTALL_REQUESTED -> IO.just(ArInstallResponse.INSTALL_REQUESTED)
            InstallStatus.INSTALLED -> IO.just(ArInstallResponse.INSTALLED)
            null -> IO.raiseError(ArError.ArCore.Apk.Unknown)
        }
}
