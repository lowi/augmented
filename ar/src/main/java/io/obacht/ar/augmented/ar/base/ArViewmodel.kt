package io.obacht.ar.augmented.ar.base

import android.Manifest
import android.app.Activity
import android.content.Context
import arrow.fx.IO
import io.obacht.ar.augmented.ar.util.ArCoreManager
import io.obacht.ar.augmented.ar.util.ArInstallResponse
import io.obacht.ar.augmented.architecture.Errors
import io.obacht.ar.augmented.architecture.Permissions
import io.obacht.ar.augmented.architecture.base.viewmodel.BaseViewModel
import io.obacht.ar.augmented.domain.ArError
import io.obacht.ar.augmented.domain.RenderableMap
import io.obacht.ar.augmented.domain.RenderableRepository

abstract class ArViewmodel<S : IArState>(
    private val fileNames: Map<String, String>,
    private val renderableRepository: RenderableRepository
) : BaseViewModel<S>() {

    fun loadRenderables(): IO<RenderableMap> = renderableRepository.loadModels(fileNames)
    private var apkInstallRequested: Boolean = false

    // request permissions of the camera
    protected fun requestCameraPermissions(context: Context): IO<Unit> =
        Permissions.check(context, Manifest.permission.CAMERA)
            .map { permission ->
                permission.fold({
                    // not good. permission was not granted.
                    IO.raiseError<Unit>(Errors.Permissions.Camera)
                }) {
                    IO.unit
                }
            }

    protected fun installApk(activity: Activity): IO<Unit> =
        ArCoreManager.installArCore(activity, apkInstallRequested)
            .map {
                when (it) {
                    ArInstallResponse.INSTALL_REQUESTED -> {
                        // obacht: side-effect
                        apkInstallRequested = true
                        IO.raiseError(ArError.ArCore.Apk.InstallationRequested)
                    }
                    ArInstallResponse.INSTALLED -> IO.unit
                }
            }

}