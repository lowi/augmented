buildscript {
    repositories {
        google()
        jcenter()

    }
    dependencies {
        classpath(io.obacht.ar.gradle.Libs.androidGradlePlugin)
        classpath(io.obacht.ar.gradle.Libs.Kotlin.gradlePlugin)
        classpath(io.obacht.ar.gradle.Libs.Sceneform.gradlePlugin)
        classpath(io.obacht.ar.gradle.Libs.Navigation.safeArgsPlugin)
    }
}


subprojects {
    repositories {
        google()
        jcenter()
//        maven(url = "https://jitpack.io")
    }

}


tasks.register<Delete>("clean") {
    delete(rootProject.buildDir)
}
