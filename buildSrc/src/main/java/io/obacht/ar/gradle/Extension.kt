package io.obacht.ar.gradle
import org.gradle.kotlin.dsl.DependencyHandlerScope

fun DependencyHandlerScope.kotlin() {
    "implementation"(Libs.Kotlin.stdlib)
    "implementation"(Libs.Coroutines.android)
    "implementation"(Libs.Kotlin.reflect)
}

fun DependencyHandlerScope.arrow() {
    "implementation"(Libs.Arrow.core)
    "implementation"(Libs.Arrow.fx)
    "kapt"(Libs.Arrow.meta)
}

fun DependencyHandlerScope.room() {
    "implementation"(Libs.Room.runtime)
    "kapt"(Libs.Room.compiler)
    "implementation"(Libs.Room.ktx)
    "androidTestImplementation"(Libs.Room.test)
}

fun DependencyHandlerScope.support() {
    "implementation"(Libs.Support.activityKtx)
    "implementation"(Libs.Support.appCompat)
    "implementation"(Libs.Support.coreKtx)
    "implementation"(Libs.Support.extensions)
    "implementation"(Libs.Support.livedataKtx)
    "implementation"(Libs.Support.viewmodelKtx)
    "implementation"(Libs.Support.viewmodelSavedState)
}

fun DependencyHandlerScope.koin() {
    "implementation"(Libs.Koin.core)
    "implementation"(Libs.Koin.android)
    "implementation"(Libs.Koin.scope)
    "implementation"(Libs.Koin.viewmodel)
    "androidTestImplementation"(Libs.Koin.test)
}

fun DependencyHandlerScope.hilt() {
    "implementation"(Libs.Hilt.core)
    "kapt"(Libs.Hilt.compiler)
    "implementation"(Libs.Hilt.xViewModel)
    "kapt"(Libs.Hilt.xCompiler)
}

fun DependencyHandlerScope.network() {
    "implementation"(Libs.Network.conscrypt)
    "implementation"(Libs.Network.okhttp)
    "implementation"(Libs.Network.okhttpLogging)
    "implementation"(Libs.Network.retrofit)
    "implementation"(Libs.Network.retrofitGson)
    "implementation"(Libs.Network.retrofitMoshi)
}

fun DependencyHandlerScope.navigation() {
    "implementation"(Libs.Navigation.fragmentKtx)
    "implementation"(Libs.Navigation.uiKtx)
    "androidTestImplementation"(Libs.Navigation.test)
}

fun DependencyHandlerScope.compose() {
    "implementation"(Libs.Compose.animation)
    "implementation"(Libs.Compose.ui)
    "implementation"(Libs.Compose.foundation)
    "implementation"(Libs.Compose.foundationLayout)
    "implementation"(Libs.Compose.material)
    "implementation"(Libs.Compose.materialIconsCore)
    "implementation"(Libs.Compose.materialIconsExt)
    "implementation"(Libs.Compose.runtime)
    "implementation"(Libs.Compose.runtimeLivedata)
    "implementation"(Libs.Compose.uiTooling)
//    "implementation"(Libs.Compose.router)
    "androidTestImplementation"(Libs.Compose.test)
    "androidTestImplementation"(Libs.Compose.uiTest)
}

fun DependencyHandlerScope.design() {
    "implementation"(Libs.Design.constraintLayout)
    "implementation"(Libs.Design.material)
}

fun DependencyHandlerScope.arCore() {
    "implementation"(Libs.ArCore.core)
    "implementation"(Libs.ArCore.obj)
}

fun DependencyHandlerScope.sceneform() {
    "implementation"(Libs.Sceneform.animation)
    "implementation"(Libs.Sceneform.core)
    "implementation"(Libs.Sceneform.ux)
}

fun DependencyHandlerScope.tests() {
    "testImplementation"(Libs.Test.junit4)
    "androidTestImplementation"(Libs.Test.testRunner)
    "androidTestImplementation"(Libs.Test.Espresso.core)
}

