package io.obacht.ar.gradle

import com.android.build.gradle.AppExtension
import com.android.build.gradle.BaseExtension
import com.android.build.gradle.LibraryExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project

class AndroidPlugin : Plugin<Project> {
    override fun apply(project: Project) {

        // Apply Required Plugins.
        project.plugins.apply("kotlin-android")
        project.plugins.apply("kotlin-android-extensions")
        project.plugins.apply("kotlin-kapt")
        // using koin for now
        // project.plugins.apply("dagger.hilt.android.plugin")

        // Configure common android build parameters.
        val androidExtension = project.extensions.getByName("android")
        if (androidExtension is BaseExtension) {

            androidExtension.apply {
                buildToolsVersion("30.0.2")
                compileSdkVersion(AndroidSdk.compile)

                defaultConfig {
                    targetSdkVersion(AndroidSdk.target)
                    minSdkVersion(AndroidSdk.min)

                    versionCode = Version.applicationVersionCode
                    versionName = Version.applicationVersionName

                    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
                }


                // Configure common proguard file settings.
                val proguardFile = "proguard-rules.pro"
                when (this) {
                    is LibraryExtension -> defaultConfig {
                        consumerProguardFiles(proguardFile)
                    }
                    is AppExtension -> buildTypes {
                        getByName("release") {
                            isMinifyEnabled = true
                            isShrinkResources = true
                            proguardFiles(
                                getDefaultProguardFile("proguard-android-optimize.txt"),
                                proguardFile
                            )
                        }
                    }
                }

                // Java 8
                compileOptions {
                    sourceCompatibility = JavaVersion.VERSION_1_8
                    targetCompatibility = JavaVersion.VERSION_1_8
                }

                dataBinding.isEnabled = true

                // See: https://developer.android.com/training/testing/set-up-project
                useLibrary("android.test.runner")
                useLibrary("android.test.base")
                useLibrary("android.test.mock")
            }
        }
    }
}
