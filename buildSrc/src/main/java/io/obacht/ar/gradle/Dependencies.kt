package io.obacht.ar.gradle

// https://codelabs.developers.google.com/codelabs/jetpack-compose-basics/#2
// When importing classes related to Jetpack Compose in this project, use those from:
// androidx.compose.* for compiler and runtime classes
// androidx.ui.* for UI toolkit and libraries
private const val xCompose = "androidx.compose"
private const val xUi = "androidx.ui"

const val kotlinVersion = "1.4.0"

internal object Version {
    const val buildTools = "4.2.0-alpha10"

    const val support = "1.3.0-alpha02"
    const val coreKtx = "1.5.0-alpha02"
    const val navigation = "2.3.0"
    const val paging = "2.1.2"
    const val coroutines = "1.3.9"

    const val compose = "1.0.0-alpha02"
    const val lifecycle = "2.3.0-alpha07"

    const val arrow = "0.10.5"
    const val koin = "2.1.6"
    const val hilt = "2.28.3-alpha"
    const val hiltX = "1.0.0-alpha02"

    const val okhttp = "4.8.0"
    const val retrofit = "2.9.0"
    const val moshi = "1.9.3"
    const val coil = "0.11.0"
    const val room = "2.2.5"

    const val sceneform = "1.17.1"
    const val filament = "1.8.1"

    const val junit4 = "4.12"
    const val xTest = "1.3.0"
    const val xJunit = "1.1.2"
    const val espresso = "3.1.0-alpha4"
//    const val robolectric = "4.4"

    // ————————————————————————————————————————————————————

    const val applicationId = "io.obacht.ar.Augment"
    const val applicationVersionCode = 1
    const val applicationVersionName = "1.0.0"
}

object AndroidSdk {
    const val min = 28
    const val compile = 30
    const val target = compile
}

object Libs {
    const val androidGradlePlugin = "com.android.tools.build:gradle:${Version.buildTools}"
    const val spotless = "com.diffplug.spotless:spotless-plugin-gradle:5.3.0"
    const val design = "com.google.android.material:material:${Version.support}"

    // Permission handling
    const val dexter = "com.karumi:dexter:6.2.1"

    // logging
    const val timber = "com.jakewharton.timber:timber:4.7.1"

    // images
    const val coil=  "io.coil-kt:coil:${Version.coil}"

    // json
    const val moshi = "com.squareup.moshi:moshi-kotlin:${Version.moshi}"


    object Kotlin {
        private const val ktln = "org.jetbrains.kotlin"
        const val stdlib = "$ktln:kotlin-stdlib-jdk8:$kotlinVersion"
        const val gradlePlugin = "$ktln:kotlin-gradle-plugin:$kotlinVersion"
        const val extensions = "$ktln:kotlin-android-extensions:$kotlinVersion"
        const val reflect = "$ktln:kotlin-reflect:$kotlinVersion"
    }

    object Coroutines {
        private const val ktlnX = "org.jetbrains.kotlinx"
        const val core = "$ktlnX:kotlinx-coroutines-core:${Version.coroutines}"
        const val android = "$ktlnX:kotlinx-coroutines-android:${Version.coroutines}"
        const val test = "$ktlnX:kotlinx-coroutines-test:${Version.coroutines}"
    }

    object Support {
        private const val life = "androidx.lifecycle"
        const val activityKtx = "androidx.activity:activity-ktx:1.2.0-alpha08"
        const val appCompat = "androidx.appcompat:appcompat:${Version.support}"
        const val coreKtx = "androidx.core:core-ktx:${Version.coreKtx}"
        const val livedataKtx = "$life:lifecycle-livedata-ktx:${Version.lifecycle}"
        const val viewmodelKtx = "$life:lifecycle-viewmodel-ktx:${Version.lifecycle}"
        const val viewmodelSavedState = "$life:lifecycle-viewmodel-savedstate:${Version.lifecycle}"
        const val extensions = "androidx.lifecycle:lifecycle-extensions:2.2.0"
    }

    object Design {
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.0"
        const val material = "com.google.android.material:material:1.2.0"
    }

    object Navigation {
        // navigation
        private const val nav = "androidx.navigation"
        const val fragmentKtx = "$nav:navigation-fragment-ktx:${Version.navigation}"
        const val uiKtx = "$nav:navigation-ui-ktx:${Version.navigation}"
        const val dynFeatures = "$nav:navigation-dynamic-features-fragment:${Version.navigation}"
        const val safeArgsPlugin = "$nav:navigation-safe-args-gradle-plugin:${Version.navigation}"
        const val test = "androidx.navigation:navigation-testing:${Version.navigation}"
    }

    object Compose {
        const val animation = "$xCompose.animation:animation:${Version.compose}"

        // Foundation (Border, Background, Box, Image, Scroll, shapes, animations, etc.)
        const val foundation = "$xCompose.foundation:foundation:${Version.compose}"
        const val foundationLayout = "$xCompose.foundation:foundation-layout:${Version.compose}"

        // Material Design
        const val material = "$xCompose.material:material:${Version.compose}"
        const val materialIconsCore = "$xCompose.material:material-icons-core:${Version.compose}"
        const val materialIconsExt = "$xCompose.material:material-icons-extended:${Version.compose}"
        const val runtime = "$xCompose.runtime:runtime:${Version.compose}"
        const val runtimeLivedata = "$xCompose.runtime:runtime-livedata:${Version.compose}"

        const val ui = "$xCompose.ui:ui:${Version.compose}"
        const val uiTooling = "$xUi:ui-tooling:${Version.compose}"

        const val test = "$xCompose.test:test-core:${Version.compose}"
        const val uiTest = "$xUi:ui-test:${Version.compose}"


        // routing
//        const val router = "com.github.zsoltk:compose-router:0.18.0"
    }

    // functional helpers
    object Arrow {
        const val meta = "io.arrow-kt:arrow-meta:${Version.arrow}"
        const val core = "io.arrow-kt:arrow-core:${Version.arrow}"
        const val fx = "io.arrow-kt:arrow-fx:${Version.arrow}"
    }

    // Dependency injection
    object Koin {
        const val core = "org.koin:koin-core:${Version.koin}"
        const val android = "org.koin:koin-android:${Version.koin}"
        const val scope = "org.koin:koin-androidx-scope:${Version.koin}"
        const val viewmodel = "org.koin:koin-androidx-viewmodel:${Version.koin}"
        const val test = "org.koin:koin-test:${Version.koin}"
    }

    object Hilt {
        const val core = "com.google.dagger:hilt-android:${Version.hilt}"
        const val compiler = "com.google.dagger:hilt-android-compiler:${Version.hilt}"
        const val xViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:${Version.hiltX}"
        const val xCompiler = "androidx.hilt:hilt-compiler:${Version.hiltX}"
    }

    object Room {
        const val runtime = "androidx.room:room-runtime:${Version.room}"
        const val compiler = "androidx.room:room-compiler:${Version.room}"
        const val ktx =  "androidx.room:room-ktx:${Version.room}"
        const val test =   "androidx.room:room-testing:${Version.room}"
    }

    object Network {
        const val conscrypt = "org.conscrypt:conscrypt-android:2.4.0"
        const val okhttp = "com.squareup.okhttp3:okhttp:${Version.okhttp}"
        const val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:${Version.okhttp}"
        const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
        const val retrofitGson = "com.squareup.retrofit2:converter-gson:${Version.retrofit}"
        const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Version.retrofit}"

    }

    object ArCore {
        const val core = "com.google.ar:core:1.18.0"
        const val obj = "de.javagl:obj:0.3.0"
    }

    object Sceneform {
        const val animation = "com.google.ar.sceneform:animation:${Version.sceneform}"
        const val core = "com.google.ar.sceneform:core:${Version.sceneform}"
        const val gradlePlugin = "com.google.ar.sceneform:plugin:${Version.sceneform}"
        const val ux = "com.google.ar.sceneform.ux:sceneform-ux:${Version.sceneform}"

    }

    object Test {
        const val junit4 = "junit:junit:${Version.junit4}"
        const val testRunner = "androidx.test:runner:${Version.xTest}"

        // Core library
        const val xCore = "androidx.test:core:${Version.xTest}"
        const val xCoreKtx = "androidx.test:core-ktx:${Version.xTest}"

        // AndroidJUnitRunner and JUnit Rules
        const val xRunner = "androidx.test:runner:${Version.xTest}"
        const val xRules = "androidx.test:rules:${Version.xTest}"

        // Assertions
        const val xJunit = "androidx.test.ext:junit:${Version.xJunit}"
        const val xJunitKtx = "androidx.test.ext:junit-ktx:${Version.xJunit}"
        const val xTruth = "androidx.test.ext:truth:${Version.xTest}"
        const val truth = "com.google.truth:truth:1.0.1"

        object Espresso {
            private const val esp = "androidx.test.espresso"
            const val core = "$esp:espresso-core:${Version.espresso}"
            const val contrib = "$esp:espresso-contrib:${Version.espresso}"
            const val intents = "$esp:espresso-intents:${Version.espresso}"
            const val accessibility = "$esp:espresso-accessibility:${Version.espresso}"
            const val web = "$esp:espresso-web:${Version.espresso}"
            const val idlingConcurrent = "$esp.idling:idling-concurrent:${Version.espresso}"

            // The following Espresso dependency can be either "const val foo = " or "AndroidTestconst val foo = ",
            // depending on whether you want the dependency to appear on your APK"s compile classpath or the test APK classpath.
            const val idlingResource = "$esp:espresso-idling-resource::${Version.espresso}"
        }
    }


}

