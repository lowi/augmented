package io.obacht.ar.gradle

import com.android.build.gradle.AppExtension
import com.android.build.gradle.BaseExtension
import com.android.build.gradle.LibraryExtension
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

class ComposePlugin : Plugin<Project> {
    override fun apply(project: Project) {

        // Configure common android build parameters.
        val androidExtension = project.extensions.getByName("android")
        if (androidExtension is BaseExtension) {

            androidExtension.apply {
                buildFeatures.compose = true

                composeOptions {
                    kotlinCompilerVersion = kotlinVersion
                    kotlinCompilerExtensionVersion = Version.compose
                }

                project.tasks.withType(KotlinCompile::class.java).configureEach {
                    kotlinOptions {
                        jvmTarget = "1.8"
                        useIR = true
                        // Treat all Kotlin warnings as errors
                        //  allWarningsAsErrors = true
                        freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
                        freeCompilerArgs += "-Xskip-prerelease-check"
                        freeCompilerArgs += "-Xopt-in=kotlin.Experimental"
                        freeCompilerArgs += "-Xallow-jvm-ir-dependencies"
                    }

                }
            }
        }
    }
}
