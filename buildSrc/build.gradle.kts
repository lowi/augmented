
plugins {
    `java-gradle-plugin`
    `kotlin-dsl`
}

gradlePlugin {
    plugins {
        register("obacht-android") {
            id = "obacht-android"
            implementationClass = "io.obacht.ar.gradle.AndroidPlugin"
        }

        register("obacht-compose") {
            id = "obacht-compose"
            implementationClass = "io.obacht.ar.gradle.ComposePlugin"
        }
    }
}

repositories {
    jcenter()
    google()
}


dependencies {
    compileOnly(gradleApi())

    implementation("com.android.tools.build:gradle:4.2.0-alpha10")
    implementation(kotlin("gradle-plugin", "1.4.0"))
    implementation(kotlin("android-extensions"))
    implementation(kotlin("reflect", "1.4.0"))
}
