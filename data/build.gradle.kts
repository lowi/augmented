import io.obacht.ar.gradle.arrow
import io.obacht.ar.gradle.koin
import io.obacht.ar.gradle.sceneform

plugins {
    id("com.android.library")
    id("obacht-android")
}

dependencies {
    implementation( project(":architecture"))
    implementation( project(":ar"))
    implementation( project(":domain"))
    arrow()
    koin()
    sceneform()
}
