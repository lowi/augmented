package io.obacht.ar.augmented.data

import android.content.res.AssetManager
import arrow.fx.IO
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Session
import io.obacht.ar.augmented.architecture.Errors
import io.obacht.ar.augmented.architecture.base.repository.ILocalDataSource
import io.obacht.ar.augmented.domain.ArError
import io.obacht.ar.augmented.domain.ImageDbDataSource
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class AssetImageDbDataSource(
    private val assetManager: AssetManager
) : ImageDbDataSource, ILocalDataSource {

    override fun loadImageDb(session: Session, filename: String): IO<AugmentedImageDatabase> =
        openInputStream("images/$filename").flatMap {
            try {
                IO.just(AugmentedImageDatabase.deserialize(session, it))
            } catch (e: IOException) {
                IO.raiseError(ArError.ArCore.ImageDB(e.localizedMessage!!))
            }
        }

    private fun stringFromAssetFile(stream: InputStream): IO<String> =
        try {
            val shaderSource = StringBuilder()
            val reader = BufferedReader(InputStreamReader(stream))
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                shaderSource.append(line).append("\n")
            }
            reader.close()
            IO.just(shaderSource.toString())
        } catch (e: IOException) {
            IO.raiseError(ArError.Gl.Shader.Load)
        }

    private fun openInputStream(path: String): IO<InputStream> =
        try {
            IO.just(assetManager.open(path))
        } catch (e: IOException) {
            IO.raiseError(Errors.Storage.NotFound(e.localizedMessage!!))
        }
}