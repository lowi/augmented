package io.obacht.ar.augmented.data.di

import io.obacht.ar.augmented.data.AssetImageDbDataSource
import io.obacht.ar.augmented.data.AssetRenderableDataSource
import io.obacht.ar.augmented.domain.ImageDbDataSource
import io.obacht.ar.augmented.domain.ImageDbRepository
import io.obacht.ar.augmented.domain.RenderableDataSource
import io.obacht.ar.augmented.domain.RenderableRepository
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module


val dataModule = module {


    single<ImageDbDataSource> {
        AssetImageDbDataSource(assetManager = androidContext().assets)
    }

    single {
        ImageDbRepository(dataSource = get())
    }

    single<RenderableDataSource> {
        AssetRenderableDataSource(context = androidContext())
    }

    single {
        RenderableRepository(dataSource = get())
    }

}



