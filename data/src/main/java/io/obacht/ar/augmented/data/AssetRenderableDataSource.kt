package io.obacht.ar.augmented.data

import android.content.Context
import android.net.Uri
import arrow.core.Either
import arrow.fx.IO
import arrow.fx.extensions.fx
import com.google.ar.sceneform.rendering.ModelRenderable
import io.obacht.ar.augmented.architecture.base.repository.ILocalDataSource
import io.obacht.ar.augmented.domain.RenderableDataSource
import io.obacht.ar.augmented.domain.RenderableRepository

class AssetRenderableDataSource(
    private val context: Context
) : RenderableDataSource {

    override fun loadModel(
        filename: String
    ): IO<ModelRenderable> = IO.async { cb ->
        ModelRenderable.builder()
            .setSource(context, Uri.parse(filename))
            .build()
            .thenAccept {
                cb(Either.right(it))
            }
            .exceptionally {
                cb(Either.left(it))
                null
            }
    }

    override fun loadModels(files: Map<String, String>): IO<Map<String, ModelRenderable>> =
        IO.fx {
            files.map {
                val (id, filename) = it
                val renderable = loadModel(filename).bind()
                Pair(id, renderable)
            }.toMap()
        }
}
