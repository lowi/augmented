import io.obacht.ar.gradle.Libs
import io.obacht.ar.gradle.arrow
import io.obacht.ar.gradle.koin

plugins {
    id("com.android.library")
    id("obacht-android")
}

dependencies {
    arrow()
    koin()
    "implementation"(Libs.timber)
    "implementation"(Libs.dexter)
}