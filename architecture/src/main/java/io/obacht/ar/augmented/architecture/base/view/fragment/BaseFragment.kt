package io.obacht.ar.augmented.architecture.base.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.obacht.ar.augmented.architecture.base.view.IView
import io.obacht.ar.augmented.architecture.base.viewmodel.IViewModel
import io.obacht.ar.augmented.architecture.base.viewstate.IViewState
import io.obacht.ar.augmented.architecture.ext.observeState



abstract class BaseFragment<S : IViewState, VM : IViewModel<S>> : Fragment(), IView {
    private var mRootView: View? = null

    abstract val layoutId: Int
    abstract val viewmodel: VM

    /**
     * Observer-function called after the state has been updated.
     */
    /*
     * The communication goes like this:
     * The viewmodel keeps the state wrapped inside LiveData
     * 1. When a state change is in order, it calls .postNext(newState)
     * 2. Android takes care of handling the update and sends an event to all observers.
     */
    abstract fun onNewState(state: S)


    // internal helper for casting the IViewState to the actual type of the state S
    @Suppress("UNCHECKED_CAST")
    private fun _onNewState(state: IViewState) = onNewState(state as S)


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mRootView = inflater.inflate(layoutId, container, false)
        return mRootView!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeState(viewmodel.stateLiveData, ::_onNewState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mRootView = null
    }
}
