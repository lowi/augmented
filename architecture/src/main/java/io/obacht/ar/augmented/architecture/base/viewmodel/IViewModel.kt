package io.obacht.ar.augmented.architecture.base.viewmodel

import androidx.lifecycle.LiveData
import io.obacht.ar.augmented.architecture.base.viewstate.IViewState

/**
 * Base type for all viewmodels.
 */
interface IViewModel<S: IViewState> {

    // The one thing they all have in common is the state
    val stateLiveData: LiveData<S>
}