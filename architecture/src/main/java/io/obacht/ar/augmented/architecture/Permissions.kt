package io.obacht.ar.augmented.architecture

import android.content.Context
import arrow.core.Either
import arrow.fx.IO
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.PermissionRequestErrorListener
import com.karumi.dexter.listener.single.PermissionListener

typealias PermissionResponse = Either<String, String>


/**
 * Permissions is a helper object for requesting hardware-permissions (e.g. use of the camera)
 * from the device.
 */
object Permissions {


    /**
     * Request a permission by means of IO.fx.
     *
     * @param context the android application context
     * @param permission the permission string
     * @return a PermissionResponse wrapped into an IO.
     */
    fun check(context: Context, permission: String): IO<PermissionResponse> =

        IO.async {
            val listener = object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) =
                    it(Either.right(Either.right(permission)))

                override fun onPermissionDenied(response: PermissionDeniedResponse) =
                    it(Either.right(Either.left(permission)))

                override fun onPermissionRationaleShouldBeShown(
                    request: PermissionRequest,
                    token: PermissionToken
                ) = it(Either.left(Errors.Permissions.Rationale(request.name)))
            }

            val errorListener = PermissionRequestErrorListener {
                it(Either.left(Errors.Permissions.Request(it.name)))
            }

            Dexter.withContext(context)
                .withPermission(permission)
                .withListener(listener)
                .withErrorListener(errorListener)
                .onSameThread()
                .check()
        }
}