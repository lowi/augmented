package io.obacht.ar.augmented.architecture.base.viewmodel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import arrow.fx.IO
import io.obacht.ar.augmented.architecture.base.viewstate.IViewState

abstract class BaseViewModel<S : IViewState> : ViewModel(), IViewModel<S> {

    // keep a mutable version of the state ready at hand
    abstract val mutableState: MutableLiveData<S>

    abstract fun initialize(activity: Activity): IO<Unit>

}