package io.obacht.ar.augmented.architecture.base.repository

interface IRepository

interface IRemoteDataSource

interface ILocalDataSource