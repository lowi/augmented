package io.obacht.ar.augmented.architecture.ext

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import io.obacht.ar.augmented.architecture.base.viewstate.IViewState
import kotlin.reflect.KFunction1


fun <T> LifecycleOwner.observe(liveData: LiveData<T>, observer: KFunction1<Any, Unit>) {
    liveData.observe(this, { it?.let { t -> observer(t) } })
}

fun <S: IViewState> LifecycleOwner.observeState(liveData: LiveData<S>, observer: KFunction1<S, Unit>) {
    liveData.observe(this, { it?.let { s -> observer(s) } })
}
