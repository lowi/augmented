package io.obacht.ar.augmented.architecture


open class Errors : Error() {

    sealed class Permissions : Errors() {
        class Request(val permission: String) : Permissions()
        class Rationale(val msg: String) : Permissions()
        object Camera : Permissions()
    }

    sealed class Storage : Errors() {
        class NotFound(val msg: String) : Storage()
        object Unknown : Storage()
    }
}

