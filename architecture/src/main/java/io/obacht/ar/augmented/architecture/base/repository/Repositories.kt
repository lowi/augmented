package io.obacht.ar.augmented.architecture.base.repository

open class BaseRepositoryBoth<T : IRemoteDataSource, R : ILocalDataSource>(
        val remoteDataSource: T,
        val localDataSource: R
) : IRepository

open class BaseRepositoryLocal<T : ILocalDataSource>(
        val dataSource: T
) : IRepository

open class BaseRepositoryRemote<T : IRemoteDataSource>(
        val dataSource: T
) : IRepository

open class BaseRepositoryNothing() : IRepository