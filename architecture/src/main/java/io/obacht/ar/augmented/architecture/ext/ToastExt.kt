package io.obacht.ar.augmented.architecture.ext

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import io.obacht.ar.augmented.architecture.R


//fun Context.toast(value: String) = toast { value }

@SuppressLint("InflateParams")
inline fun Context.toast(value: () -> String) {
//    val inflater: LayoutInflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
//    val view: View = inflater.inflate(R.layout.toast, null)

//    val image: ImageView = view.findViewById(R.id.image) as ImageView
//    image.setImageResource(R.drawable.android)

//    val text = view.findViewById(R.id.text) as TextView
//    text.text = value()

    val toast = Toast(applicationContext)
    toast.duration = Toast.LENGTH_SHORT
    toast.setText(value())
//    toast.view = view
    toast.show()
}



