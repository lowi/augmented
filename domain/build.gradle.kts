import io.obacht.ar.gradle.arrow
import io.obacht.ar.gradle.sceneform

plugins {
    id("com.android.library")
    id("obacht-android")
}

dependencies {
    implementation( project(":architecture"))
//    implementation( project(":ar"))
    arrow()
//    koin()
    sceneform()
}

