package io.obacht.ar.augmented.domain

import arrow.fx.IO
import com.google.ar.core.AugmentedImageDatabase
import com.google.ar.core.Session
import io.obacht.ar.augmented.architecture.base.repository.BaseRepositoryLocal
import io.obacht.ar.augmented.architecture.base.repository.ILocalDataSource

interface ImageDbDataSource : ILocalDataSource {
    fun loadImageDb(session: Session, filename: String): IO<AugmentedImageDatabase>
}

class ImageDbRepository(
    dataSource: ImageDbDataSource,
) : BaseRepositoryLocal<ImageDbDataSource>(dataSource) {

    fun loadImageDb(session: Session, filename: String): IO<AugmentedImageDatabase> =
        dataSource.loadImageDb(session, filename)

}
