package io.obacht.ar.augmented.domain

import io.obacht.ar.augmented.architecture.Errors


sealed class ArError : Errors() {

    sealed class ArCore : ArError() {
        class ImageDB(val msg: String) : ArCore()
        object NotSupported : ArCore()
        object NotInstalled : ArCore()
        object OutOfDate : ArCore()
        object Unknown : ArCore()

        sealed class Apk : ArCore() {
            object InstallationRequested : ArCore()
            object Unknown : Apk()
            object UnknownChecking : Apk()
            object UnknownTimeout : Apk()
            object DeviceIncapable : Apk()
            object NotInstalled : Apk()
            object TooOld : Apk()
        }
    }

    sealed class Gl : ArError() {
        sealed class Asset : Gl() {
            class NotFound(val msg: String) : Asset()
        }

        sealed class Shader : ArError() {
            object Load : Shader()
            class Compile(val msg: String) : Shader()
        }
    }

}

