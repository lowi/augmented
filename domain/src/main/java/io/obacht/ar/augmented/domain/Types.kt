package io.obacht.ar.augmented.domain

import com.google.ar.core.AugmentedImage
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.rendering.ModelRenderable

typealias ImageSceneMap = Map<AugmentedImage, AnchorNode>
typealias RenderableMap = Map<String, ModelRenderable>