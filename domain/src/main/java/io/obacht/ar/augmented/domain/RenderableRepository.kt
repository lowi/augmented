package io.obacht.ar.augmented.domain

import arrow.fx.IO
import com.google.ar.sceneform.rendering.ModelRenderable
import io.obacht.ar.augmented.architecture.base.repository.ILocalDataSource

interface RenderableDataSource : ILocalDataSource {
    fun loadModel(filename: String): IO<ModelRenderable>
    fun loadModels(files: Map<String, String>): IO<Map<String, ModelRenderable>>
}


class RenderableRepository(
    private val dataSource: RenderableDataSource,
) {

    fun loadModel(filename: String): IO<ModelRenderable> =
        dataSource.loadModel(filename)

    fun loadModels(files: Map<String, String>): IO<RenderableMap> =
        dataSource.loadModels(files)
}
