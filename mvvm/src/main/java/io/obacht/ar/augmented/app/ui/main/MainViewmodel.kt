package io.obacht.ar.augmented.app.ui.main

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import arrow.fx.IO
import io.obacht.ar.augmented.architecture.base.viewmodel.BaseViewModel

class MainViewmodel : BaseViewModel<MainState>() {

    // this is ugly. need to differentiate between state that is relevant for the view and
    // other state-information (which we can just keep in a local var.)
    // todo: clean this up
    private var state = MainState.Loading
    override val mutableState: MutableLiveData<MainState> = MutableLiveData(state)
    override val stateLiveData: LiveData<MainState> = mutableState

    override fun initialize(activity: Activity): IO<Unit> = IO.unit


}