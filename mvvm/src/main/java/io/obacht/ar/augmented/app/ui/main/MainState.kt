package io.obacht.ar.augmented.app.ui.main

import io.obacht.ar.augmented.architecture.base.viewstate.IViewState


sealed class MainState : IViewState {
    object Loading : MainState()
    object Ready : MainState()
}
