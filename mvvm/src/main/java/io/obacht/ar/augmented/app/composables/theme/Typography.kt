package io.obacht.ar.augmented.app.composables.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.font
import androidx.compose.ui.text.font.fontFamily
import androidx.compose.ui.unit.sp
import io.obacht.ar.augmented.R

private val PlexMonoFontFamily = fontFamily(
    font(R.font.ibmplexmono_regular, FontWeight.Normal, FontStyle.Normal),
    font(R.font.ibmplexmono_italic, FontWeight.Normal, FontStyle.Italic),

    font(R.font.ibmplexmono_bold, FontWeight.Bold, FontStyle.Normal),
    font(R.font.ibmplexmono_bolditalic, FontWeight.Bold, FontStyle.Italic),

    font(R.font.ibmplexmono_extralight, FontWeight.ExtraLight, FontStyle.Normal),
    font(R.font.ibmplexmono_extralightitalic, FontWeight.ExtraLight, FontStyle.Italic),

    font(R.font.ibmplexmono_light, FontWeight.Light, FontStyle.Normal),
    font(R.font.ibmplexmono_lightitalic, FontWeight.Light, FontStyle.Italic),

    font(R.font.ibmplexmono_medium, FontWeight.Medium, FontStyle.Normal),
    font(R.font.ibmplexmono_mediumitalic, FontWeight.Medium, FontStyle.Italic),

    font(R.font.ibmplexmono_semibold, FontWeight.SemiBold, FontStyle.Normal),
    font(R.font.ibmplexmono_semibolditalic, FontWeight.SemiBold, FontStyle.Italic),

    font(R.font.ibmplexmono_thin, FontWeight.Thin, FontStyle.Normal),
    font(R.font.ibmplexmono_thinitalic, FontWeight.Thin, FontStyle.Italic),
)

private val PlexSerifFamily = fontFamily(
    font(R.font.ibmplexserif_regular, FontWeight.Normal, FontStyle.Normal),
    font(R.font.ibmplexserif_italic, FontWeight.Normal, FontStyle.Italic),

    font(R.font.ibmplexserif_bold, FontWeight.Bold, FontStyle.Normal),
    font(R.font.ibmplexserif_bolditalic, FontWeight.Bold, FontStyle.Italic),

    font(R.font.ibmplexserif_extralight, FontWeight.ExtraLight, FontStyle.Normal),
    font(R.font.ibmplexserif_extralightitalic, FontWeight.ExtraLight, FontStyle.Italic),

    font(R.font.ibmplexserif_light, FontWeight.Light, FontStyle.Normal),
    font(R.font.ibmplexserif_lightitalic, FontWeight.Light, FontStyle.Italic),

    font(R.font.ibmplexserif_medium, FontWeight.Medium, FontStyle.Normal),
    font(R.font.ibmplexserif_mediumitalic, FontWeight.Medium, FontStyle.Italic),

    font(R.font.ibmplexserif_semibold, FontWeight.SemiBold, FontStyle.Normal),
    font(R.font.ibmplexserif_semibolditalic, FontWeight.SemiBold, FontStyle.Italic),

    font(R.font.ibmplexserif_thin, FontWeight.Thin, FontStyle.Normal),
    font(R.font.ibmplexserif_thinitalic, FontWeight.Thin, FontStyle.Italic),

    )

val JetchatTypography = Typography(
    defaultFontFamily = PlexMonoFontFamily,
    h1 = TextStyle(
        fontWeight = FontWeight.Light,
        fontSize = 96.sp,
        letterSpacing = (-1.5).sp
    ),
    h2 = TextStyle(
        fontWeight = FontWeight.Light,
        fontSize = 60.sp,
        letterSpacing = (-0.5).sp
    ),
    h3 = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 48.sp,
        letterSpacing = 0.sp
    ),
    h4 = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 30.sp,
        letterSpacing = 0.sp
    ),
    h5 = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 24.sp,
        letterSpacing = 0.sp
    ),
    h6 = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp,
        letterSpacing = 0.sp
    ),
    subtitle1 = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp,
        letterSpacing = 0.sp
    ),
    subtitle2 = TextStyle(
        fontFamily = PlexSerifFamily,
        fontWeight = FontWeight.Bold,
        fontSize = 14.sp,
        letterSpacing = 0.1.sp
    ),
    body1 = TextStyle(
        fontFamily = PlexSerifFamily,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        letterSpacing = 0.sp,
        lineHeight = 24.sp
    ),
    body2 = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = 14.sp,
        letterSpacing = 0.25.sp
    ),
    button = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 14.sp,
        letterSpacing = 1.25.sp
    ),
    caption = TextStyle(
        fontFamily = PlexSerifFamily,
        fontWeight = FontWeight.Bold,
        fontSize = 12.sp,
        letterSpacing = 0.15.sp
    ),
    overline = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 12.sp,
        letterSpacing = 1.sp
    )
)
