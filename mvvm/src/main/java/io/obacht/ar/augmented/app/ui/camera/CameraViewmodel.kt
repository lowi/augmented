package io.obacht.ar.augmented.app.ui.camera

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import arrow.core.Option
import arrow.fx.IO
import arrow.fx.extensions.fx
import com.google.ar.core.AugmentedImage
import com.google.ar.core.Session
import com.google.ar.core.TrackingState
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Scene
import io.obacht.ar.augmented.ar.ext.konfigure
import io.obacht.ar.augmented.ar.image.ArImageViewmodel
import io.obacht.ar.augmented.ar.image.AugmentedImageNode
import io.obacht.ar.augmented.architecture.logger.AppLogger
import io.obacht.ar.augmented.domain.ImageDbRepository
import io.obacht.ar.augmented.domain.RenderableRepository

const val IMAGEDB_FILE = "images.imgdb"

//
//class ProfileViewModel : ViewModel() {
//    private var userId: String = ""
//    fun setUserId(newUserId: String?) {
//        if (newUserId != userId) {
//            userId = newUserId ?: meProfile.userId
//        }
//        _userData.value = if (userId == meProfile.userId) meProfile else colleagueProfile
//    }
//    private val _userData = MutableLiveData<ProfileScreenState>()
//    val userData: LiveData<ProfileScreenState> = _userData
//}
//
//@Immutable
//data class ProfileScreenState(
//    val userId: String,
//    @DrawableRes val photo: Int?,
//    val name: String,
//    val status: String,
//    val displayName: String,
//    val position: String,
//    val twitter: String = "",
//    val timeZone: String?, // Null if me
//    val commonChannels: String? // Null if me
//) {
//    fun isMe() = userId == meProfile.userId
//}


class CameraViewmodel(
    fileNames: Map<String, String>,
    renderableRepository: RenderableRepository,
    imageDbRepository: ImageDbRepository,
) : ArImageViewmodel<CameraState>(fileNames, renderableRepository, imageDbRepository) {

    // this is ugly. need to differentiate between state that is relevant for the view and
    // other state-information (which we can just keep in a local var.)
    // todo: clean this up
    private var state = CameraState()
    override val mutableState: MutableLiveData<CameraState> = MutableLiveData(state)
    override val stateLiveData: LiveData<CameraState> = mutableState

    fun openDrawer() {
        AppLogger.i("viewmodel open drawer")
    }


    override fun initialize(activity: Activity): IO<Unit> =

        // ask for permission to use the camera
        requestCameraPermissions(activity)

            // make sure the arCore apk is installed
            .followedBy(installApk(activity))

            .followedBy(
                IO.fx {
                    val renderables = loadRenderables().bind()

                    val session = Session(activity)
                    val imageDB = imageDbRepository.loadImageDb(session, IMAGEDB_FILE).bind()
                    session.konfigure(imageDB)

                    state = state
                        .updateSession(session)
                        .updateRenderables(renderables)
                    mutableState.postValue(state)
                }
            )


    fun onUpdateFrame(frameTime: FrameTime, scene: Scene, images: Collection<AugmentedImage>) {
        for (image in images) {
            @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
            when (image.trackingState) {
                TrackingState.PAUSED -> {
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
//                    toast { "Detected Image ${image.index}" }
                    AppLogger.i("not found ${image.name}")
                }
                TrackingState.TRACKING -> {

                    // Create a new anchor for newly found images.
                    if (!state.anchorMap.containsKey(image)) {
                        Option.fromNullable(state.renderables[image.name])
                            .fold({ AppLogger.i("not found ${image.name}") })
                            {
                                val node = AugmentedImageNode(it, image)
                                scene.addChild(node)
                                state = state.addImageNode(image, node)
                            }


                    }
                }

                TrackingState.STOPPED -> state = state.removeImageNode(image)
            }
        }
    }
}