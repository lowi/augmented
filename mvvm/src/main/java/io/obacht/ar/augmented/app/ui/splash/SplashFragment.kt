package io.obacht.ar.augmented.app.ui.splash

import io.obacht.ar.augmented.R
import io.obacht.ar.augmented.architecture.base.view.fragment.BaseFragment
import io.obacht.ar.augmented.architecture.logger.AppLogger
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : BaseFragment<SplashState, SplashViewmodel>() {

    override val layoutId: Int = R.layout.fragment_splash
    override val viewmodel: SplashViewmodel by viewModel()


    override fun onNewState(state: SplashState) {
        AppLogger.i("Splash— onNewState: $state")
    }

}