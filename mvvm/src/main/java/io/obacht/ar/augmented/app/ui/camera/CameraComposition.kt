package io.obacht.ar.augmented.app.ui.camera

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.ContextAmbient
import androidx.compose.ui.viewinterop.AndroidView
import arrow.core.Option
import com.google.ar.core.AugmentedImage
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.FrameTime
import io.obacht.ar.augmented.architecture.logger.AppLogger

@Composable
fun CameraView(
    onUpdateFrame: (FrameTime) -> Unit
) {
    val selectedItem = remember { mutableStateOf(0) }

    val context = ContextAmbient.current
    val customView = remember {
        // Creates custom view
        ArSceneView(context).apply {
            AppLogger.i("ArSceneView: $this")
            this.scene.addOnUpdateListener(onUpdateFrame)
//            this.scene.addOnUpdateListener { onUpdateFrame(it) }
        }
    }

    AndroidView({ customView }) { view ->
        // View's been inflated - add logic here if necessary
        AppLogger.i("android view: $view")


        // As selectedItem is read here, AndroidView will recompose
        // whenever the state changes
        // Example of Compose -> View communication
//        view.coordinator.selectedItem = selectedItem
    }
}



///**
// * Registered with the Sceneform Scene object, this method is called at the start of each frame.
// *
// * @param frameTime - time since last frame.
// */
//private fun onUpdateFrame(frameTime: FrameTime) {
//    Option.fromNullable(arSceneView.arFrame).fold({}) { frame ->
//        viewmodel.onUpdateFrame(
//            frameTime,
//            arSceneView.scene,
//            frame.getUpdatedTrackables(AugmentedImage::class.java)
//        )
//    }
//}