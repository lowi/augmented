package io.obacht.ar.augmented.app.ui.splash

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import arrow.fx.IO
import io.obacht.ar.augmented.architecture.base.viewmodel.BaseViewModel

class SplashViewmodel : BaseViewModel<SplashState>() {

    // this is ugly. need to differentiate between state that is relevant for the view and
    // other state-information (which we can just keep in a local var.)
    // todo: clean this up
    private var state = SplashState()
    override val mutableState: MutableLiveData<SplashState> = MutableLiveData(state)
    override val stateLiveData: LiveData<SplashState> = mutableState

    override fun initialize(activity: Activity): IO<Unit> = IO.unit


}