package io.obacht.ar.augmented.app.ui.camera

import arrow.core.Option
import com.google.ar.core.AugmentedImage
import com.google.ar.core.Session
import io.obacht.ar.augmented.ar.image.ArImageState
import io.obacht.ar.augmented.ar.image.AugmentedImageNode
import io.obacht.ar.augmented.ar.image.IArImageState
import io.obacht.ar.augmented.domain.RenderableMap

data class CameraState(
    val imageState: ArImageState = ArImageState(),
    val anchorMap: Map<AugmentedImage, AugmentedImageNode> = HashMap()
) : IArImageState {

    fun updateRenderables(renderables: RenderableMap): CameraState {
        val arState = this.imageState.arState.copy(renderables = renderables)
        val imageState = this.imageState.copy(arState = arState)
        return this.copy(imageState = imageState)
    }

    fun updateSession(session: Session): CameraState {
        val arState = this.imageState.arState.copy(arSession = Option.just(session))
        val imageState = this.imageState.copy(arState = arState)
        return this.copy(imageState = imageState)
    }

    fun addImageNode(image: AugmentedImage, node: AugmentedImageNode): CameraState =
        this.copy(anchorMap = anchorMap + Pair(image, node))

    fun removeImageNode(image: AugmentedImage): CameraState =
        this.copy(anchorMap = anchorMap.filter { (i, _) ->
            i.name != image.name
        })

    val arSession = imageState.arState.arSession
    val renderables = imageState.arState.renderables
}