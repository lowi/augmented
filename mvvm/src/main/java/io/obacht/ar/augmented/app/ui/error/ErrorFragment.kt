package io.obacht.ar.augmented.app.ui.error

import io.obacht.ar.augmented.R
import io.obacht.ar.augmented.architecture.base.view.fragment.BaseFragment
import io.obacht.ar.augmented.architecture.logger.AppLogger
import org.koin.androidx.viewmodel.ext.android.viewModel

class ErrorFragment : BaseFragment<ErrorState, ErrorViewmodel>() {

    override val layoutId: Int = R.layout.fragment_splash
    override val viewmodel: ErrorViewmodel by viewModel()


    override fun onNewState(state: ErrorState) {
        AppLogger.i("Error— onNewState: $state")
    }

}