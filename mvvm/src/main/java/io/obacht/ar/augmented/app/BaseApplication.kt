package io.obacht.ar.augmented.app

import android.app.Application
import io.obacht.ar.augmented.BuildConfig
import io.obacht.ar.augmented.app.di.appModule
import io.obacht.ar.augmented.architecture.logger.AppLogger
import io.obacht.ar.augmented.data.di.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber


open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        loadKoin()
        AppLogger.init(BuildConfig.DEBUG)
    }

    private fun loadKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@BaseApplication)
            modules(dataModule)
            modules(appModule)
//            androidFileProperties()
//            modules(glModule)
//            modules(loginModule)
        }
    }


    companion object {
        lateinit var INSTANCE: BaseApplication
    }
}