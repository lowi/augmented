package io.obacht.ar.augmented.app.ui.camera

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import arrow.core.Option
import com.google.ar.core.AugmentedImage
import com.google.ar.core.Session
import com.google.ar.sceneform.ArSceneView
import com.google.ar.sceneform.FrameTime
import io.obacht.ar.augmented.R
import io.obacht.ar.augmented.app.ui.MainActivity
import io.obacht.ar.augmented.ar.base.ArFragment
import io.obacht.ar.augmented.architecture.logger.AppLogger
import org.koin.androidx.viewmodel.ext.android.viewModel


@Suppress("PLUGIN_WARNING")
@SuppressLint("CheckResult")
class CameraFragment : ArFragment<CameraState, CameraViewmodel>() {

    override val layoutId: Int = R.layout.fragment_camera
    override val sceneViewId: Int = R.id.sceneform_view

    override val viewmodel: CameraViewmodel by viewModel()

    // local state received from the ViewState after the viewmodel has initialized
    // keeping it here makes the checks in onRestart/onPause easier
    private var arSession: Option<Session> = Option.empty()

    private lateinit var arSceneView: ArSceneView

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewmodel.initialize(requireActivity())
            .unsafeRunAsync {
                it.fold({ err ->
                    AppLogger.e("error: ${err.localizedMessage!!}")
                }) {
                    AppLogger.i("all is well")
                }
            }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        arSceneView = view.findViewById(sceneViewId)
        arSceneView.scene.addOnUpdateListener { onUpdateFrame(it) }

        val button: ImageButton = view.findViewById(R.id.drawer_button)
        button.setOnClickListener {
            AppLogger.i("clickk")
            (requireActivity() as MainActivity).openDrawer()
        }

        return view
    }


    override fun onResume() {
        super.onResume()
        start()
    }

    override fun onPause() {
        super.onPause()
        stop()
    }

    override fun onDestroy() {
        arSession.fold({}) {
            arSceneView.pause()
            arSceneView.destroy()
            it.pause()
        }
        super.onDestroy()
    }

    override fun onNewState(state: CameraState) {
        AppLogger.i("onNewState: $state")
        arSession = state.arSession
        arSession.fold({ /*initialize()*/ }) {
            arSceneView.setupSession(it)
            start()
        }
    }


    private fun start() {
        arSession.fold({ /*initialize()*/ }) {
            // obacht: order is important
            it.resume()
            arSceneView.resume()
        }
    }

    private fun stop() {
        arSession.fold({}) {
            // obacht: order is important
            arSceneView.pause()
            it.pause()
        }
    }

    /**
     * Registered with the Sceneform Scene object, this method is called at the start of each frame.
     *
     * @param frameTime - time since last frame.
     */
    private fun onUpdateFrame(frameTime: FrameTime) {
        Option.fromNullable(arSceneView.arFrame).fold({}) { frame ->
            viewmodel.onUpdateFrame(
                frameTime,
                arSceneView.scene,
                frame.getUpdatedTrackables(AugmentedImage::class.java)
            )
        }
    }
}