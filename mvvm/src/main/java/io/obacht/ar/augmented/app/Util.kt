package io.obacht.ar.augmented.app

import io.obacht.ar.augmented.architecture.ext.toast


inline fun toast(value: () -> String): Unit = BaseApplication.INSTANCE.toast(value)