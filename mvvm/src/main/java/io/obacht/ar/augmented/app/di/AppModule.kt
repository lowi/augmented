package io.obacht.ar.augmented.app.di

import io.obacht.ar.augmented.app.ui.error.ErrorViewmodel
import io.obacht.ar.augmented.app.ui.camera.CameraViewmodel
import io.obacht.ar.augmented.app.ui.splash.SplashViewmodel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

// This is a pre-created database containing the sample image.
//private const val SAMPLE_IMAGE_DATABASE = "sample_database.imgdb"

val appModule = module {

    single<Map<String, String>> {
        val map: HashMap<String, String> = HashMap()
        map["fiducial 1"] = "models/frame_upper_left.sfb"
        map
    }

    // Viewmodels
    // ——————————
    viewModel {
        SplashViewmodel()
    }

    viewModel {
        ErrorViewmodel()
    }

    viewModel {
        CameraViewmodel(
            fileNames = get(),
            renderableRepository = get(),
            imageDbRepository = get(),
        )
    }

}



