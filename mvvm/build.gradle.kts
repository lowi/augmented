import io.obacht.ar.gradle.*

plugins {
    id("com.android.application")
    id("obacht-android")
    id("obacht-compose")
    id("com.google.ar.sceneform.plugin")
    id("androidx.navigation.safeargs")
}

dependencies {

    implementation(project(":architecture"))
    implementation( project(":ar"))
    implementation( project(":data"))
    implementation (project(":domain"))

    "implementation"(Libs.timber)
    arrow()
    compose()
    design()
    koin()
    kotlin()
    navigation()
    sceneform()
    support()

}


sceneform.asset(
    "sampledata/models/andy.obj", // "Source Asset Path" specified during import.
    "default",                    // "Material Path" specified during import.
    "sampledata/models/andy.sfa", // ".sfa Output Path" specified during import.
    "src/main/res/raw/andy")      // ".sfb Output Path" specified during import.

